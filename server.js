const express = require("express");
const app = express();
const contacto = require("./contacto.json");
const fs = require("fs");
const path = require("path");
const hbs = require("hbs");
hbs.registerPartials(path.join(__dirname, "views", "partials"));
app.set("view engine", "hbs"); // clave valor

hbs.registerHelper("getCurrentYear", () => new Date().getFullYear());
// con paso de parámetro:
hbs.registerHelper("toUpperCase", text => text.toUpperCase());

const staticRoute = path.join(__dirname, "public");
app.use(express.static(staticRoute));

app.use((req, res, next) => {
  var now = new Date().toString();
  var log = `${now}: ${req.method} ${req.url}`;
  console.log(log);
  fs.appendFile("server.log", `${log}\n`, err => {
    if (err) console.log(`No se ha podido usar el fichero de log:  ${err}`);
  });
  next();
});

app.get("/contacto", (req, res) => {
  res.send(contacto);
});
app.listen(3000);

app.get("/contactar", (req, res) => {
  res.render("contactar.hbs", {
    pageTitle: "Contactar",
    currentYear: new Date().getFullYear()
  });
});
